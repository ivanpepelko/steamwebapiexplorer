function display_method(mdata) {
    method_data = mdata;
    $('#send_request').data('method_data', mdata);
    $('#http_method').text(mdata.method.httpmethod);
    $('#breadcrumb_iface').text(mdata.interface);
    $('#breadcrumb_method').text(mdata.method.name);
    $('#breadcrumb_version').text('v' + mdata.method.version);

    $.post('/method_form', mdata, function (data) {
        $('#method_form').html(data);
    });
    $('#response').hide();

    if (mdata.method.httpmethod.toLowerCase() === 'get') {
        $('#send_request').removeAttr('disabled');
    } else {
        $('#send_request').attr('disabled', 'disabled');
    }

}

var method_data = {
    interface: "ISteamWebAPIUtil",
    method: {
        httpmethod: "GET",
        name: "GetSupportedAPIList",
        parameters: [{
                description: "access key",
                name: "key",
                optional: true,
                type: "string"
            }],
        version: 1}
};

formSubmitHandler = function (e) {
    e.preventDefault();
    $('#working').show();
    $('#send_request').button('loading');
    $.ajax('/make_request', {
        data: $.extend(method_data, {parameters: $('#method_form form').serialize()}),
        method: 'post',
        beforeSend: function () {
            $('.progress-bar').css('width', 0).text(null);
            $('.progress').show();
        },
        xhrFields: {
            onprogress: function (p) {
                if (p.lengthComputable) {
                    var percentage = (p.loaded / p.total) * 0.5;
                    console.log('percnet', percentage);
                    $('.progress-bar').css('width', (percentage * 100) + '%').text('Loading... ' + parseInt(percentage * 100) + '%');
                } else {
                    console.log('lengthComputable=' + p.lengthComputable);
                }
            }
        },
        success: function (data, s, xhr) {
            //$('#response').jsonViewer(JSON.parse(data));
            var r = $('#response');
            r.html(null);
            $('.progress-bar').css('width', 0);
            //console.log(data);
            var current = 0, splitBy = 1024 * 512, busy = false;
            var timer = setInterval(
                    function () {
                        if (!busy) {
                            busy = true;
                            r[0].innerHTML += data.substr(current, splitBy);
                            //r.add(data.substr(current, splitBy));
                            var percentage = ((current + splitBy) / data.length) * 0.5 + 0.5;
                            console.log('percflush', percentage);
                            $('.progress-bar').css('width', (percentage * 100) + '%').text('Flushing to view... ' + parseInt(percentage * 100) + '%');
                            current += splitBy;
                            if (current > data.length) {
                                r.show();
                                $('#working').hide();
                                $('#send_request').button('reset');
                                setTimeout(function () {
                                    $('.progress').hide();
                                }, 750);
                                clearInterval(timer);
                            }
                            busy = false;
                        }
                    }, 50);
        },
        error: function (xhr, status, desc) {
            $('#response').text("An error occured!\n" + desc);
            $('#response').show();
            $('#working').hide();
            $('#send_request').button('reset');
            $('.progress').hide();
        }
    });
    return false;
};

$(function () {
    $('a[data-method_data]').on('click', function () {
        display_method($(this).data('method_data'));
    });

    $('#method_form').on('submit', '#params_form', formSubmitHandler);
    $('#send_request').on('click', formSubmitHandler);
    $('#send_request').click();

    $('#response').on('click', 'a.json-toggle', function () {
        var target = $(this).toggleClass('collapsed').siblings('ul.json-dict, ol.json-array');
        target.toggle();
        if (target.is(':visible')) {
            target.siblings('.json-placeholder').remove();
        }
        else {
            var count = target.children('li').length;
            var placeholder = count + (count > 1 ? ' items' : ' item');
            target.after('<a href class="json-placeholder">' + placeholder + '</a>');
        }
        return false;
    });

    $('#response').on('click', 'a.json-placeholder', function () {
        $(this).siblings('a.json-toggle').click();
        return false;
    });

    var methods = [];
    $.each(apilist.apilist.interfaces, function () {
        var interface = this.name;
        $.each(this.methods, function () {
            methods.push({
                interface: interface,
                method: this
            });
        });
    });

    var suggestions = new Bloodhound({
        datumTokenizer: function (method) {
            return Bloodhound.tokenizers.whitespace(
                    method.interface.split(/(?=[A-Z])|_/)
                    .concat(method.method.name.split(/(?=[A-Z])/))
                    .concat([method.interface, method.method.name])
                    .join(" "));
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: methods
    });

    $('.typeahead').typeahead({
        hint: false,
        highlight: true,
        minLength: 1
    }, {
        name: 'method',
        source: suggestions,
        limit: 15,
        display: function (datum) {
            return (datum.interface + '::' + datum.method.name + ' (V' + datum.method.version + ')');
        }
    }).on('typeahead:select', function (e, data) {
        display_method(data);
        $(this).blur();
    }).on('focus', function () {
        $(this).select();
    });


    $(document.body).on('mouseover', '.tt-suggestion', function () {
        $(this).closest('.tt-menu').find('.tt-cursor').removeClass('tt-cursor').end().end().addClass('tt-cursor');
    });

});