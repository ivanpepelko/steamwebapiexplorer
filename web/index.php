<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Swapi\Config;
use Swapi\Util;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

$app = new Application();
$app->register(new TwigServiceProvider());
new Config($app);

$app->get('/', function (Request $req, Application $app) {
    $apikey = $req->cookies->get('apikey');
    return $app['twig']->render('index.html.twig', [
                'apilist' => Util::getApiList($apikey),
                'apikey' => $apikey
    ]);
});

$app->post('/setapikey', function(Request $req, Application $app) {
    $apikey = $app->escape($req->get('apikey'));
    $res = new RedirectResponse('/');
    $res->headers->setCookie(new Cookie('apikey', $apikey));
    return $res;
});

$app->post('/make_request', function(Request $req) {
    $apikey = $req->cookies->get('apikey');
    $interface = $req->get('interface');
    $method = $req->get('method')['name'];
    $version = $req->get('method')['version'];
    $parameters = $req->get('parameters');
    $content = Util::getFromSteamApi($apikey, $interface, $method, $version, $parameters);

    //$j2h = new Json2Html($content);
    //$html = $j2h->getHtml();

    $contentLength = strlen($content);
    
    $res = new Symfony\Component\HttpFoundation\Response($content);
    $res->headers->set('Content-Type', 'text/plain');
    $res->headers->set('Content-Length', $contentLength);
    
    return $res;
});

$app->post('/method_form', function(Request $req, Application $app) {
    $parameters = $req->get('method')['parameters'];
    return $app['twig']->render('method-form.html.twig', [
        'parameters' => $parameters
    ]);
});

$app->run();
