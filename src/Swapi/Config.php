<?php

namespace Swapi;

use Silex\Application;

/**
 * Description of Settings
 *
 * @author Ivan Pepelko <ivan.pepelko@gmail.com>
 */
class Config {

    private $app;

    public function __construct(Application $app) {
        $this->app = $app;
        $app['twig.path'] = __DIR__ . '/views';
        $app['debug'] = true;
    }

    public function get($name) {
        return $this->app[$name];
    }

    public function set($name, $value) {
        $this->app[$name] = $value;
        return $this;
    }

}
