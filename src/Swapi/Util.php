<?php

namespace Swapi;

class Util {

    public static function getApiList($apikey = null) {
        $ch = curl_init("http://api.steampowered.com/ISteamWebAPIUtil/GetSupportedAPIList/v1/?key=$apikey");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $apilist = json_decode(curl_exec($ch));
        curl_close($ch);
        return $apilist;
    }

    public static function getFromSteamApi($apikey, $interface, $method, $version, $params = "") {
        $url = "http://api.steampowered.com/$interface/$method/v$version/?key=$apikey&$params";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
